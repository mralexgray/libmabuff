/*
Copyright (C) 2012-2013 Adam Matoušek

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#ifndef _SNAKE_H_
#define _SNAKE_H_

#include "snake.hpp"
#include "view.hpp"
#include <cstdlib>

using std::list;
using std::set;

Snake *snake = 0;
Field *field = 0;
bool done = false;

Snake::Snake(){
  atoms.push_front(418);
  atoms.push_front(419);
  atoms.push_front(420);
  atoms.push_front(421);
  atoms.push_front(422);
  direction = E;
}

void Snake::redirect(Direction ndir){
  if((direction == N || direction == S) && (ndir == N || ndir == S)) return;
  if((direction == E || direction == W) && (ndir == E || ndir == W)) return;
  else direction = ndir; return;
}

void Snake::move(){
  unsigned dest = atoms.front();
  switch(direction){
    case N: dest -= field->get_cols(); break;
    case E: dest++; break;
    case S: dest += field->get_cols(); break;
    case W: dest--; break;
  }
  if(field->check(dest) && !is_atom(dest)){
    atoms.push_front(dest);
    atoms.pop_back();
  }
  else done = true;
}

void Snake::grow(){
  atoms.push_back(atoms.back());
}

void Snake::draw(){
  for (list<unsigned>::iterator i = atoms.begin(); i != atoms.end(); i++)
    draw_atom(*i);
}

bool Snake::is_atom(unsigned pos){
  for (list<unsigned>::iterator i = atoms.begin(); i != atoms.end(); i++)
    if(*i == pos) return 1; //Is an atom
  return 0; //Not an atom
}


Field::Field(unsigned col, unsigned row):columns(col), rows(row){
  // Make borders invalid positions:
  unsigned u;
  for(u = 0; u < col; u++) invalidate(u);
  for(u = 0; u < col; u++) invalidate((row * col - 1) - u);
  for(u = 0; u < row; u++) invalidate(col * u);
  for(u = 0; u < row; u++) invalidate(col * (u + 1) - 1);
}

unsigned Field::get_cols(){ return columns; }
unsigned Field::get_rows(){ return rows; }

bool Field::check(unsigned pos){
  if(invalids.count(pos)) return 0;
  else if (pos == food){
    points++;
    snake->grow();
    put_food();
  }
  return 1;
}

void Field::invalidate(unsigned pos){
  invalids.insert(pos);
}

void Field::draw(){
  for (set<unsigned>::iterator i = invalids.begin(); i != invalids.end(); i++)
    draw_invalid(*i);
  draw_food(food);
  draw_points(points);
}

void Field::put_food(){
  do{
    food = rand() % (columns * rows);
  } while(invalids.count(food) || snake->is_atom(food));
}
#endif

