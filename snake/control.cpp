/*
Copyright (C) 2012-2013 Adam Matoušek

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#include "control.hpp"
#include "snake.hpp"
#include "view.hpp"
#include <mabuff/input.hpp>
#include <mabuff/flags.hpp>
#include <ctime>
#include <cstdlib>
using namespace mabuff;

double freq = 0.1;
void input(){
  bool needed = false;
  do{
    needed = false;
    switch(getkey(freq)){
      case 'h':
      case key_arrow_left:
      case 'a': snake->redirect(W); break;

      case 'j':
      case key_arrow_down:
      case 's': snake->redirect(S); break;

      case 'k':
      case key_arrow_up:
      case 'w': snake->redirect(N); break;

      case 'l':
      case key_arrow_right:
      case 'd': snake->redirect(E); break;

      case 'q': done = true; break;
      case 'p': draw_pause(); getkey(); draw_pause(0); needed = true; break;

      default: break;
    }
  } while(needed);
}

void move(){
  snake->move();
}

void prepare(){
  srand(time(NULL));
  field = new Field(40, 20);
  snake = new Snake();
  field->put_food();
}

void refreq(double nfreq){
  freq = nfreq;
}

double get_freq(){
  return freq;
}

