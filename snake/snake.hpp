/*
Copyright (C) 2012-2013 Adam Matoušek

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#include <list>
#include <set>

enum Direction{
  N, E, S, W
};

class Snake{
  private:
    std::list<unsigned> atoms;
    Direction direction;
  public:
    Snake();
    void redirect(Direction ndir);
    void move();
    void grow();
    void draw();
    bool is_atom(unsigned pos);
};

class Field{
  private:
    std::set<unsigned> invalids; //positions, that cannot be entered
    unsigned columns;
    unsigned rows;
    unsigned food; //position of current food
    unsigned points; //number of food eaten
  public:
    Field(unsigned col, unsigned row);
    unsigned get_cols();
    unsigned get_rows();
    bool check(unsigned pos); //check position's validity (true = valid)
    void invalidate(unsigned pos); //make the position invalid
    void draw(); //highlight invalid positions and draw food
    void put_food();
};

extern Snake *snake;
extern Field *field;
extern bool done;

