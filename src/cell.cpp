/*
Copyright (C) 2012-2013 Adam Matoušek

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#include "cell.hpp"
#include "termbox.hpp"
#include "mabuff.hpp"

namespace mabuff{

  const char applicable_bitmask = white | adopt | bold | terminal;
    // ^ What a cell stores
  const char properties_bitmask = white | bold | terminal;
    // ^ What is transfered to termbox

  Cell::Cell(const int & _x, const int & _y){
    move(_x, _y);
    change_char(adopt_char);
    change_fg(adopt);
    change_bg(adopt);
  }

  void Cell::move(int abs_x, int abs_y){
    x = abs_x;
    y = abs_y;
  }

  void Cell::move_by(int rel_x, int rel_y){// + right/down; - left/up
    x += rel_x;
    y += rel_y;
  }

  void Cell::change_char(const unsigned & ch_new){
    if((ch_new > 31 && ch_new != 127) || ch_new == invisible) ch = ch_new;
    else if(ch_new == adopt_char) ch = adopt;
  }

  void Cell::change_fg(const unsigned & flags_new){
    if(flags_new & keep){
      if((flags_new & bold) && !(fg_flags & bold)) fg_flags += bold;
      else if((flags_new & unbold) && (fg_flags & bold)) fg_flags -= bold;
      fg_flags |= flags_new & applicable_bitmask;
    }
    else{ fg_flags = flags_new & applicable_bitmask; }
  }

  void Cell::change_bg(const unsigned & flags_new){
    if(flags_new & keep) bg_flags |= flags_new & applicable_bitmask;
    else bg_flags = flags_new & applicable_bitmask;
  }

  //void Cell::apply_mask(const char mask);

  void Cell::print(){
    if(x >= 0 && y >= 0 && x < Root->get_width() && y < Root->get_height() &&
        x < tb::width() && y < tb::height())
      tb::change_cell(x, y, (ch == adopt || ch == invisible)?(tb::get_cell(x, y)->ch):(ch),
      (fg_flags & adopt || ch == invisible)?(tb::get_cell(x, y)->fg | ((ch == invisible)?(0):(fg_flags & properties_bitmask))):(fg_flags & properties_bitmask),
      (bg_flags & adopt || ch == invisible)?(tb::get_cell(x, y)->bg | ((ch == invisible)?(0):(bg_flags & properties_bitmask))):(bg_flags & properties_bitmask));
  }
}
