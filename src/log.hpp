/*
Copyright (C) 2012-2013 Adam Matoušek

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#ifndef _MAB_LOG_H_
#define _MAB_LOG_H_ 0

/* Logging of MABuffer, levels:
    0) NONE - Do not use as logging level
    1) CRIT - Followed by abortion of the Buffer
    2) ERR  - Followed by weird behaviour
    3) WARN - Unusual circumstances have occured
    4) INFO - Debugging, common behaviour
*/

namespace mabuff{

  enum LogLevel{
    NONE = 0,
    CRIT = 1,
    ERR  = 2,
    WARN = 3,
    INFO = 4
  };

  extern LogLevel Verbosity;

  void Log(LogLevel lvl, const char* str);

}

#endif
