/*
Copyright (C) 2012-2013 Adam Matoušek

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#ifndef _MAB_FLAGS_H_
#define _MAB_FLAGS_H_ 0

namespace mabuff{

  // FG and BG flags
  enum{
      black    = 0x00,  // code: n (e.g.: noir, nothing, null)
      red      = 0x01,  // code: r
      green    = 0x02,  // code: g
      yellow   = 0x03,  // code: y
      blue     = 0x04,  // code: b
      magenta  = 0x05,  // code: m
      cyan     = 0x06,  // code: c
      white    = 0x07,  // code: w

      // The terminal's default. Doesn't blend with other flags
      terminal = 0x80,  // code: t

      bold     = 0x10,  // code: (uppercase color code if default is not bold)
      //Only works if 'keep' or 'adopt' is set. Forces not-boldness:
      unbold   = 0x40,  // code: (uppercase color code if default is bold)

      //Cells ignore request for color change, but bold/unbold still works If
      //'keep' and none of bold/unbold are set, then the bold setting is kept
      //as well
      keep     = 0x20,

      //The flag is extracted from the cell below. Bold/unbold apply as with
      //'keep'.  You can use it as character, too. Both flags and character are
      //dynamically adopting the values on each print, so feel free to move the
      //zones underneath.
      adopt    = 0x08,
  };

  // Text flags
  enum{
    oneline  = 1, //One-line text, ignore \n etc, unfitting text is discarded

    //Aligns
    /*
       When using these flags, start_x parameter in Buffer::write()
       will define an distance from the selected border (left, right),
       or an offset if 'center' is used (+ left, - right).
     */
    right  = 128,
    left   = 64,
    center = right|left,
  };

  // Special characters
  enum{
    // On the position of this character, the cell won't be
    //rewritten.  Only usable in the Zone::insert() and Zone::write().
    skip = '\t',
    invisible = '\v', // The cell in the zone underneath is shown
    fg_changer = '\f',// Change foreground to code after this (see codes above)
    bg_changer = '\b',// Change background to code after this (see codes above)
    adopt_char = '\a' // Description in the first enum (bg & fg flags)
  };

  // KEYS
  enum {

// keys
  key_f1    = (0xFFFF-0),
  key_f2    = (0xFFFF-1),
  key_f3    = (0xFFFF-2),
  key_f4    = (0xFFFF-3),
  key_f5    = (0xFFFF-4),
  key_f6    = (0xFFFF-5),
  key_f7    = (0xFFFF-6),
  key_f8    = (0xFFFF-7),
  key_f9    = (0xFFFF-8),
  key_f10    = (0xFFFF-9),
  key_f11    = (0xFFFF-10),
  key_f12    = (0xFFFF-11),
  key_insert  = (0xFFFF-12),
  key_delete  = (0xFFFF-13),
  key_home  = (0xFFFF-14),
  key_end    = (0xFFFF-15),
  key_pgup  = (0xFFFF-16),
  key_pgdn  = (0xFFFF-17),
  key_arrow_up  = (0xFFFF-18),
  key_arrow_down  = (0xFFFF-19),
  key_arrow_left  = (0xFFFF-20),
  key_arrow_right  = (0xFFFF-21),

  key_ctrl_tilde    = 0x00,
  key_ctrl_2    = 0x00,
  key_ctrl_a    = 0x01,
  key_ctrl_b    = 0x02,
  key_ctrl_c    = 0x03,
  key_ctrl_d    = 0x04,
  key_ctrl_e    = 0x05,
  key_ctrl_f    = 0x06,
  key_ctrl_g    = 0x07,
  key_backspace    = 0x08,
  key_ctrl_h    = 0x08,
  key_tab      = 0x09,
  key_ctrl_i    = 0x09,
  key_ctrl_j    = 0x0a,
  key_ctrl_k    = 0x0b,
  key_ctrl_l    = 0x0c,
  key_enter    = 0x0d,
  key_ctrl_m    = 0x0d,
  key_ctrl_n    = 0x0e,
  key_ctrl_o    = 0x0f,
  key_ctrl_p    = 0x10,
  key_ctrl_q    = 0x11,
  key_ctrl_r    = 0x12,
  key_ctrl_s    = 0x13,
  key_ctrl_t    = 0x14,
  key_ctrl_u    = 0x15,
  key_ctrl_v    = 0x16,
  key_ctrl_w    = 0x17,
  key_ctrl_x    = 0x18,
  key_ctrl_y    = 0x19,
  key_ctrl_z    = 0x1a,
  key_esc      = 0x1b,
  key_ctrl_lsq_bracket  = 0x1b,
  key_ctrl_3    = 0x1b,
  key_ctrl_4    = 0x1c,
  key_ctrl_backslash  = 0x1c,
  key_ctrl_5    = 0x1d,
  key_ctrl_rsq_bracket  = 0x1d,
  key_ctrl_6    = 0x1e,
  key_ctrl_7    = 0x1f,
  key_ctrl_slash    = 0x1f,
  key_ctrl_underscore  = 0x1f,
  key_space    = 0x20,
  key_backspace2    = 0x7f,
  key_ctrl_8    = 0x7f
};

}
#endif
