/*
Copyright (C) 2012-2013 Adam Matoušek

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#include "zone.hpp"
#include "termbox.hpp"
#include "mabuff.hpp"
#include "log.hpp"
#include <algorithm>
#include <sstream>
#include <iostream>

namespace mabuff{  

  extern std::multimap<int, Zone*> zones;
  void parse_changer(unsigned & fl, char ch, bool isbold);

  std::vector<unsigned> str_to_unicode(const char* input){
    std::vector<unsigned> output;
    unsigned tmp = 0;
    while(*input != '\0'){
      input += utf8_char_to_unicode(&tmp, input);
      output.push_back(tmp);
    }
    return output;
  }

  Zone::Zone(const unsigned & cols, const unsigned & rows):
  layer(0), visible(true), x(0), y(0), autohandled(1), width(cols), height(rows){
    for(unsigned _y = 0; _y < rows; _y++){
      for(unsigned _x = 0; _x < cols; _x++){
        cells.push_back(new Cell(_x, _y));
      }
    }
  }

  Zone::~Zone(){
    if (!autohandled) zones.erase(misa);
    std::vector<Cell*>::iterator it;
    for(it = cells.begin(); it != cells.end(); it++) delete *it;
    cells.clear();
  }

  Zone::Zone(const unsigned & cols, const unsigned & rows, int layer_):
  Zone(cols, rows){
    misa = zones.insert(std::make_pair(layer_, this));
    autohandled = 0;
  }

  bool Zone::is_terminator(unsigned a) const{
    if(a == '\0') return true; //Null
    else if(a == '') return true; //End of Text
    else if(a == '') return true; //End of Transmission
    else if(a == '') return true; //End of Transmission Block
    else if(a == '') return true; //End of Medium
    else if(a == '') return true; //File Separator
    else if(a == '') return true; //Group Separator
    else if(a == '') return true; //Record Separator
    else if(a == '') return true; //Unit Separator
    //else if(a == '') return true; //Your very own terminator :)
    else return false;
  }

  void Zone::fill(const unsigned & fill){
    if(fill < 31 && fill != adopt_char && fill != invisible) return;
    std::vector<Cell*>::iterator it;
    for(it = cells.begin(); it != cells.end(); it++) (*it)->change_char(fill);
  }

  void Zone::fill(const char * pattern){
    std::vector<unsigned> fill= str_to_unicode(pattern);
    unsigned patlen = fill.size(); //faster than asking every cycle's part
    if(!patlen) return;
    std::vector<Cell*>::iterator it;
    unsigned i = 0;
    for(it = cells.begin(); it != cells.end();i++, it++)
      if(fill.at(i%patlen) > 31 || fill.at(i%patlen) == adopt_char ||
          fill.at(i%patlen) == invisible)
        (*it)->change_char(fill.at(i%patlen));
  }

  void Zone::change_fg(const unsigned & flags_new){
    std::vector<Cell*>::iterator it;
    for(it = cells.begin(); it != cells.end(); it++) (*it)->change_fg(flags_new);
  }

  void Zone::change_bg(const unsigned & flags_new){
    std::vector<Cell*>::iterator it;
    for(it = cells.begin(); it != cells.end(); it++) (*it)->change_bg(flags_new);
  }

  void Zone::print() const{
    if(!visible) return;
    std::vector<Cell*>::const_iterator it;
    for(it = cells.begin(); it != cells.end(); it++) (*it)->print();
  }

  unsigned Zone::get_width() const{
    return width;
  }

  unsigned Zone::get_height() const{
    return height;
  }

  int Zone::get_x() const{
    return x;
  }

  int Zone::get_y() const{
    return y;
  }

  void parse_changer(unsigned & fl, char ch, bool isbold){
    switch(ch){
      case 'n': fl = (isbold?bold:0) | black; break;
      case 'r': fl = (isbold?bold:0) | red; break;
      case 'g': fl = (isbold?bold:0) | green; break;
      case 'y': fl = (isbold?bold:0) | yellow; break;
      case 'b': fl = (isbold?bold:0) | blue; break;
      case 'm': fl = (isbold?bold:0) | magenta; break;
      case 'c': fl = (isbold?bold:0) | cyan; break;
      case 'w': fl = (isbold?bold:0) | white; break;
      case 't': fl = (isbold?bold:0) | terminal; break;
      case 'N': fl = (isbold?unbold:bold) | black; break;
      case 'R': fl = (isbold?unbold:bold) | red; break;
      case 'G': fl = (isbold?unbold:bold) | green; break;
      case 'Y': fl = (isbold?unbold:bold) | yellow; break;
      case 'B': fl = (isbold?unbold:bold) | blue; break;
      case 'M': fl = (isbold?unbold:bold) | magenta; break;
      case 'C': fl = (isbold?unbold:bold) | cyan; break;
      case 'W': fl = (isbold?unbold:bold) | white; break;
      case 'T': fl = (isbold?unbold:bold) | terminal; break;
    }
  }

  void Zone::insert(std::istream & input, const unsigned & fg_flags, const unsigned & bg_flags){
    unsigned pos = 0;
    unsigned fg_flags_l = fg_flags;
    unsigned bg_flags_l = bg_flags;
    unsigned ch;
    char temp[6]; // Each UTF-8 character has up to six bytes (though only four are commonly used)
    unsigned char charlen = 1; // Number of bytes in character

    ch = input.get();
    while(!input.eof() && pos < cells.size()){
      // UTF-8 processing
      charlen = utf8_char_length(ch);
      if(charlen > 1){
        temp[0] = ch;
        for(int i = 1; i < charlen; i++) temp[i] = input.get();
        utf8_char_to_unicode(&ch, temp);
      }

      // Actual inserting
      if(ch > 31 && ch != 127 || ch == invisible || ch == adopt_char){
        cells[pos]->change_char(ch);
        cells[pos]->change_fg(fg_flags_l);
        cells[pos]->change_bg(bg_flags_l);
        pos++;
      }
      else if(ch == skip) pos++;
      else if(ch == fg_changer){
        ch = input.get();
        parse_changer(fg_flags_l, ch, fg_flags & bold);
      }
      else if(ch == bg_changer){
        ch = input.get();
        parse_changer(bg_flags_l, ch, 0);
      }

      ch = input.get();

      // Test for termining characters
      if(is_terminator(ch)) break;
    } //while
  }

  void Zone::insert(std::vector<unsigned> & input, const unsigned & fg_flags, const unsigned & bg_flags){
    unsigned pos = 0;
    unsigned fg_flags_l = fg_flags;
    unsigned bg_flags_l = bg_flags;
    unsigned ch;
    std::vector<unsigned>::iterator from = input.begin(); 

    ch = *from;
    while(from != input.end() && pos < cells.size()){

      if(ch > 31 && ch != 127 || ch == invisible || ch == adopt_char){
        cells[pos]->change_char(ch);
        cells[pos]->change_fg(fg_flags_l);
        cells[pos]->change_bg(bg_flags_l);
        pos++;
      }
      else if(ch == skip) pos++;
      else if(ch == fg_changer){
        ch = *++from;
        parse_changer(fg_flags_l, ch, fg_flags & bold);
      }
      else if(ch == bg_changer){
        ch = *++from;
        parse_changer(bg_flags_l, ch, 0);
      }

      ch = *++from;

      // Test for termining characters
      if(is_terminator(ch)) break;
    } //while
  }

  void Zone::insert(const char* str, const unsigned & fg_flags, const unsigned & bg_flags){
    std::vector<unsigned> tmp = str_to_unicode(str);
    insert(tmp, fg_flags, bg_flags);
  }

  void Zone::write (std::vector<unsigned> & text, const unsigned & start_x, const unsigned & start_y,
      const unsigned & text_flags, const unsigned & fg_flags, const unsigned & bg_flags){
    unsigned fg_flags_l = fg_flags;
    unsigned bg_flags_l = bg_flags;
    unsigned pos = 0;
    int dst_to_lb = 0; // Distance to next linebreak/most distant space/EOT, excluding.

    for(std::vector<unsigned>::iterator it = text.begin();
        it != text.end() && (start_y*width+pos) < cells.size(); it++){

      if(pos%width == 0){ // Have just line-broken (x = 0)
        if(pos && text_flags & oneline ) break;
        if(text_flags & left && !(text_flags & right)) pos += start_x;

        if(*it == ' ' || *it == '\n') it++; // We landed on a space or linebreak

        std::vector<unsigned>::iterator lb = it + (width - start_x);
        if(std::distance(it, text.end()) > (width - start_x)){
          lb = it + (width - start_x);
          while(!(*lb == ' ' || *lb == '\n')){
            lb--;
            if(lb == it){
              lb = it + (width - start_x);
              break;
            }
          }
        }
        else lb = text.end();

        //checks for any manual linebreaks
        std::vector<unsigned>::iterator lb2 = std::find(it, lb, '\n');
        dst_to_lb = std::distance(it, lb2);

        if(text_flags & right){ // "center" passes as well
          pos += (width - (start_x + dst_to_lb))/(bool(text_flags & left) + 1);
        }
      }

      //Actions...
      if(dst_to_lb == 0) pos += width-(pos%width);// Manual linebreak
      else if((*it > 31 && *it != 127) || *it == invisible || *it == adopt_char){
        cells[start_y*width+pos]->change_char(*it);
        cells[start_y*width+pos]->change_fg(fg_flags_l);
        cells[start_y*width+pos]->change_bg(bg_flags_l);
        pos++;
        dst_to_lb--;
      }
      else if(*it == skip) pos++;
      else if(*it == fg_changer){
        it++;
        parse_changer(fg_flags_l, *it, fg_flags & bold);
      }
      else if(*it == bg_changer){
        it++;
        parse_changer(bg_flags_l, *it, 0);
      }

    }
  }

  void Zone::write(const char* str, const unsigned & start_x, const unsigned & start_y,
      const unsigned & text_flags, const unsigned & fg_flags, const unsigned & bg_flags){
    std::vector<unsigned> text = str_to_unicode(str);
    write(text, start_x, start_y, text_flags, fg_flags, bg_flags);
  }

  void Zone::set_border(unsigned fg_flags, unsigned bg_flags, unsigned top_left,
      unsigned horiz, unsigned top_right, unsigned vert,
      unsigned bottom_left, unsigned bottom_right){
    for(unsigned i = 1; i < width; i++){ //Top and bottom lines
      cells[i]->change_char(horiz);     cells[width*(height-1)+i]->change_char(horiz);
      cells[i]->change_fg(fg_flags);    cells[width*(height-1)+i]->change_fg(fg_flags);
      cells[i]->change_bg(bg_flags);    cells[width*(height-1)+i]->change_bg(bg_flags);
    }
    for(unsigned i = 1; i < height; i++){ //Left and right lines
      cells[i*width]->change_char(vert);      cells[(i+1)*width-1]->change_char(vert);
      cells[i*width]->change_fg(fg_flags);    cells[(i+1)*width-1]->change_fg(fg_flags);
      cells[i*width]->change_bg(bg_flags);    cells[(i+1)*width-1]->change_bg(bg_flags);
    }
    cells[0]->change_char(top_left);  cells[width-1]->change_char(top_right);
    cells[0]->change_fg(fg_flags);    cells[width-1]->change_fg(fg_flags);
    cells[0]->change_bg(bg_flags);    cells[width-1]->change_bg(bg_flags);

    cells[width*(height-1)]->change_char(bottom_left);   cells.back()->change_char(bottom_right);
    cells[width*(height-1)]->change_fg(fg_flags);        cells.back()->change_fg(fg_flags);
    cells[width*(height-1)]->change_bg(bg_flags);        cells.back()->change_bg(bg_flags);
  }

  void Zone::set_border(BorderSetting *setting, unsigned fg_flags, unsigned bg_flags){
    set_border(fg_flags, bg_flags,
      setting->top_left,
      setting->horizontal,
      setting->top_right,
      setting->vertical,
      setting->bottom_left,
      setting->bottom_right
    );
  }

  void Zone::move(int abs_x, int abs_y){
    move_by(abs_x - x, abs_y - y);
  }
  void Zone::move_by(int rel_x, int rel_y){
    for(std::vector<Cell*>::iterator i = cells.begin(); i != cells.end(); i++) (*i)->move_by(rel_x, rel_y);
    x += rel_x;
    y += rel_y;
  }

  void Zone::set_visibility(bool v){
    visible = v;
  }
  bool Zone::is_visible() const{
    return visible;
  }

}
